﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// References_Trivia gathers information from game objects to be used at a later
/// point in the game.
/// @Author: Bradley Cesario, 9/9/2017
/// </summary>

public class References_Trivia : MonoBehaviour
{
  [HideInInspector]
  public Text correctAnswer;
  [HideInInspector]
  public Text incorrectAnswer;
  [HideInInspector]
  public Text questionText;
  [HideInInspector]
  public Text choiceOne;
  [HideInInspector]
  public Text choiceTwo;
  [HideInInspector]
  public Text choiceThree;
  [HideInInspector]
  public Text choiceFour;
  [HideInInspector]
  public List<string> questions = new List<string>();
  [HideInInspector]
  public List<string> answers = new List<string>();

  void Gather()
  {
    questions = GetComponent<LoadTextScript>().questions;
    answers = GetComponent<LoadTextScript>().answers;
  }

	// Use this for initialization
	void Start ()
  {
    correctAnswer = GameObject.Find("Correct Amount Text").GetComponent<Text>();
    incorrectAnswer = GameObject.Find("Incorrect Amount Text").GetComponent<Text>();
    questionText = GameObject.Find("Question").GetComponent<Text>();
    choiceOne = GameObject.Find("Choice 1").GetComponent<Text>();
    choiceTwo = GameObject.Find("Choice 2").GetComponent<Text>();
    choiceThree = GameObject.Find("Choice 3").GetComponent<Text>();
    choiceFour = GameObject.Find("Choice 4").GetComponent<Text>();

    SendMessage("Begin");
	}
}
