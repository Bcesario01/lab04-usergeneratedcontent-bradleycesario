﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[RequireComponent(typeof(References))]
public class LoadScript : MonoBehaviour
{
  FileInfo originalFile;
  TextAsset textFile;
  TextReader reader;

  public List<string> sentences = new List<string>();
  public List<string> clues = new List<string>();

  // Use this for initialization
  void Start()
  {
    textFile = (TextAsset)Resources.Load("embedded", typeof(TextAsset));
    reader = new StringReader(textFile.text);

    string lineOfText;
    int lineNumber = 0;

    //Tell the reader to read a line of text, and store that in the lineOfText variable.
    //Continue doing this until there are no lines left.
    while ((lineOfText = reader.ReadLine()) != null)
    {
      if(lineNumber%2 == 0)
      {
        //even lines
        sentences.Add(lineOfText);
      }
      else
      {
        //odd lines
        clues.Add(lineOfText);
      }

      lineNumber++;

    }

    SendMessage("Gather");
  }
	
	// Update is called once per frame
	void Update () 
  {
		
	}
}
