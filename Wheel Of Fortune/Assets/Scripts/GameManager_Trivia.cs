﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// GameManager_Trivia sets up the game, checks player input on questions.
/// @Author: Bradley Cesario, 9/9/2017
/// </summary>

[RequireComponent(typeof(References_Trivia))]
public class GameManager_Trivia : MonoBehaviour
{

  References_Trivia refs;
  int correct;
  int incorrect;
  string question;
  string answer;

  void Begin ()
  {
    refs = gameObject.GetComponent<References_Trivia>();
    StartGame();
  }

  public void StartGame()
  {
    int numSent = refs.questions.Count;
    int randomSent = Random.Range(0, numSent);

    question = refs.questions[randomSent];
    answer = refs.answers[randomSent];

    string[] multipleChoice = question.Split(',');

    refs.questionText.text = multipleChoice[0];
    refs.choiceOne.text = multipleChoice[1];
    refs.choiceTwo.text = multipleChoice[2];
    refs.choiceThree.text = multipleChoice[3];
    refs.choiceFour.text = multipleChoice[4];
  }

  public void _CheckAnswer(string button)
  {
    if( button == answer)
    {
      correct++;
      refs.correctAnswer.text = correct.ToString();
      StartGame();
    }
    else
    {
      incorrect++;
      refs.incorrectAnswer.text = incorrect.ToString();
      StartGame();
    }
  }
}
