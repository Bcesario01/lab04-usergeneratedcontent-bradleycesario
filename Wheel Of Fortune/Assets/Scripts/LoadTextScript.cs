﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[RequireComponent(typeof(References_Trivia))]
public class LoadTextScript : MonoBehaviour
{
  FileInfo originaFile;
  TextAsset textFile;
  TextReader reader;

  public List<string> questions = new List<string>();
  public List<string> answers = new List<string>();

	// Use this for initialization
	void Start ()
  {
    textFile = (TextAsset)Resources.Load("embeddedTwo", typeof(TextAsset));
    reader = new StringReader(textFile.text);

    string lineOfText;
    int lineNumber = 0;

    while ((lineOfText = reader.ReadLine()) != null)
    {
      if(lineNumber%2 == 0)
      {
        questions.Add(lineOfText);
      }
      else
      {
        answers.Add(lineOfText);
      }

      lineNumber++;
    }

    SendMessage("Gather");
	}
}
