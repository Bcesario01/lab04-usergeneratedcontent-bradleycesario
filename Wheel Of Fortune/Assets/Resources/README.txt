In order to supply questions and answers of your own, you need to enter the question
followed by a comma on the first line. After the comma you supply the potential answers
separated by commas. On the next line you input the number corresponding to the correct
answer to the question you just made.